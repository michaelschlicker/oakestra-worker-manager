package clients

import (
	"encoding/json"
	"errors"
	"io"
	"net/http"
	clustermanager "oakestra-aws-provider/dto/cluster-manager"
	"oakestra-aws-provider/logger"
	"os"
	"strconv"
	// "oakestra-aws-provider/errors"
)

type ClusterManager struct {
	httpClient *http.Client
}

func NewClusterManager(httpClient *http.Client) ClusterManager {
	return ClusterManager{
		httpClient: httpClient,
	}
}

func getClusterManagerUrl() string {
	return "http://" + os.Getenv("CLUSTER_ADDRESS") + ":10100"
}

func (client ClusterManager) RegisterNodes(count int) ([]clustermanager.RegisterNodeResponse, error) {
	logger.DebugLogger().Printf("Cluster Manager Register Nodes")
	endpoint := getClusterManagerUrl() + "/api/node/add/" + strconv.Itoa(count)
	request, err := http.NewRequest(http.MethodPost, endpoint, nil)
	if err != nil {
		return []clustermanager.RegisterNodeResponse{}, err
	}
	request.Header.Set("Content-Type", "application/json")
	logger.DebugLogger().Printf("Request: %v", request)
	response, err := client.httpClient.Do(request)
	if err != nil {
		return []clustermanager.RegisterNodeResponse{}, err
	}
	defer response.Body.Close()
	logger.DebugLogger().Printf("Register Nodes Status: %v", response.StatusCode)
	if response.StatusCode >= 400 {
		body, err := io.ReadAll(response.Body)
		errMessage := ""
		if err == nil {
			errMessage = string(body)
		}
		return []clustermanager.RegisterNodeResponse{}, errors.New(errMessage)
		// return clustermanager.RegisterNodeResponse{}, errors.HTTPError{StatusCode: response.StatusCode, ContextMessage: "System Manager couldn't register a cluster: " + errMessage}
	}
	var responseBody []clustermanager.RegisterNodeResponse
	err = json.NewDecoder(response.Body).Decode(&responseBody)
	if err != nil {
		return []clustermanager.RegisterNodeResponse{}, err
	}

	return responseBody, err
}

func (client ClusterManager) ReissueToken(nodeId string) (clustermanager.ReissueTokenResponse, error) {
	logger.DebugLogger().Printf("Cluster Manager Reissue Node Token")
	endpoint := getClusterManagerUrl() + "/api/node/" + nodeId + "/token"
	request, err := http.NewRequest(http.MethodPost, endpoint, nil)
	if err != nil {
		return clustermanager.ReissueTokenResponse{}, err
	}
	request.Header.Set("Content-Type", "application/json")
	logger.DebugLogger().Printf("Request: %v", request)
	response, err := client.httpClient.Do(request)
	if err != nil {
		return clustermanager.ReissueTokenResponse{}, err
	}
	defer response.Body.Close()
	logger.DebugLogger().Printf("Reissue Node Token Status: %v", response.StatusCode)
	if response.StatusCode >= 400 {
		body, err := io.ReadAll(response.Body)
		errMessage := ""
		if err == nil {
			errMessage = string(body)
		}
		return clustermanager.ReissueTokenResponse{}, errors.New(errMessage)
	}
	var responseBody clustermanager.ReissueTokenResponse
	err = json.NewDecoder(response.Body).Decode(&responseBody)
	if err != nil {
		return clustermanager.ReissueTokenResponse{}, err
	}

	return responseBody, err
}

func (client ClusterManager) UndeployNode(nodeId string) error {
	logger.DebugLogger().Printf("Cluster Manager Undeploy Node Jobs")
	endpoint := getClusterManagerUrl() + "/api/node/" + nodeId + "/undeploy"
	request, err := http.NewRequest(http.MethodPost, endpoint, nil)
	if err != nil {
		return err
	}
	request.Header.Set("Content-Type", "application/json")
	logger.DebugLogger().Printf("Request: %v", request)
	response, err := client.httpClient.Do(request)
	if err != nil {
		return err
	}
	defer response.Body.Close()
	logger.DebugLogger().Printf("Undeploy Node Status: %v", response.StatusCode)
	if response.StatusCode >= 400 {
		body, err := io.ReadAll(response.Body)
		errMessage := ""
		if err == nil {
			errMessage = string(body)
		}
		return errors.New(errMessage)
	}
	return nil
}
