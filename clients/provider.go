package clients

import (
	"bytes"
	"encoding/json"
	"errors"
	"io"
	"net/http"
	"oakestra-aws-provider/dto/provider"
	"oakestra-aws-provider/logger"
	"os"
)

type Provider struct {
	httpClient *http.Client
	url        string
}

func NewProvider(httpClient *http.Client) Provider {
	return Provider{
		httpClient: httpClient,
		url:        os.Getenv("PROVIDER_HOST") + ":" + os.Getenv("PROVIDER_PORT"),
	}
}

func (client Provider) CreateWorker(requestBody provider.CreateWorkerRequest) ([]string, error) {
	endpoint := "http://" + client.url + "/worker"
	logger.DebugLogger().Printf("endpoint %v", endpoint)
	body, err := json.Marshal(requestBody)
	if err != nil {
		return []string{}, err
	}
	logger.DebugLogger().Printf("body %v", body)
	request, err := http.NewRequest(http.MethodPost, endpoint, bytes.NewBuffer(body))
	if err != nil {
		return []string{}, err
	}

	request.Header.Set("Content-Type", "application/json")

	response, err := client.httpClient.Do(request)
	if err != nil {
		return []string{}, err
	}
	if response.StatusCode != http.StatusCreated {
		body, err := io.ReadAll(response.Body)
		errMessage := ""
		if err == nil {
			errMessage = string(body)
		}
		logger.DebugLogger().Printf("request %v", response.Request)
		return []string{}, errors.New(errMessage)
	}

	var responseObj provider.CreateWorkerResponse
	err = json.NewDecoder(response.Body).Decode(&responseObj)

	return responseObj.InstanceIds, err
}

func (client Provider) DeleteWorker(requestBody provider.InstanceStopRequest) error {
	endpoint := "http://" + client.url + "/worker"
	body, err := json.Marshal(requestBody)
	if err != nil {
		return err
	}
	request, err := http.NewRequest(http.MethodDelete, endpoint, bytes.NewBuffer(body))
	if err != nil {
		return err
	}

	request.Header.Set("Content-Type", "application/json")

	response, err := client.httpClient.Do(request)
	if err != nil {
		return err
	}
	if response.StatusCode != http.StatusOK {
		body, err := io.ReadAll(response.Body)
		errMessage := ""
		if err == nil {
			errMessage = string(body)
		}
		return errors.New(errMessage)
	}
	return nil
}

func (client Provider) StopWorker(requestBody provider.InstanceStopRequest) error {
	endpoint := "http://" + client.url + "/worker/stop"
	body, err := json.Marshal(requestBody)
	if err != nil {
		return err
	}
	request, err := http.NewRequest(http.MethodPost, endpoint, bytes.NewBuffer(body))
	if err != nil {
		return err
	}

	request.Header.Set("Content-Type", "application/json")

	response, err := client.httpClient.Do(request)
	if err != nil {
		return err
	}
	if response.StatusCode != http.StatusOK {
		body, err := io.ReadAll(response.Body)
		errMessage := ""
		if err == nil {
			errMessage = string(body)
		}
		return errors.New(errMessage)
	}
	return nil
}

func (client Provider) RestartWorker(requestBody provider.InstanceRestartRequest) error {
	endpoint := "http://" + client.url + "/worker/stop"
	body, err := json.Marshal(requestBody)
	if err != nil {
		return err
	}
	request, err := http.NewRequest(http.MethodPost, endpoint, bytes.NewBuffer(body))
	if err != nil {
		return err
	}

	request.Header.Set("Content-Type", "application/json")

	response, err := client.httpClient.Do(request)
	if err != nil {
		return err
	}
	if response.StatusCode != http.StatusOK {
		body, err := io.ReadAll(response.Body)
		errMessage := ""
		if err == nil {
			errMessage = string(body)
		}
		return errors.New(errMessage)
	}
	return nil
}

func (client Provider) CreateFirewall(requestBody provider.CreateFirewallRequest) (string, error) {
	logger.DebugLogger().Printf("create firewall for %v", requestBody)
	endpoint := "http://" + client.url + "/firewall"
	body, err := json.Marshal(requestBody)
	if err != nil {
		return "", err
	}
	request, err := http.NewRequest(http.MethodPost, endpoint, bytes.NewBuffer(body))
	if err != nil {
		return "", err
	}

	request.Header.Set("Content-Type", "application/json")

	response, err := client.httpClient.Do(request)
	if err != nil {
		return "", err
	}
	if response.StatusCode != http.StatusCreated {
		body, err := io.ReadAll(response.Body)
		errMessage := ""
		if err == nil {
			errMessage = string(body)
		}
		return "", errors.New(errMessage)
	}

	var responseObj provider.CreateFirewallResponse
	err = json.NewDecoder(response.Body).Decode(&responseObj)
	if err != nil {
		return "", err
	}

	return responseObj.FirewallID, nil
}

func (client Provider) AssignFirewall(requestBody provider.AssignFirewallRequest) error {
	logger.DebugLogger().Printf("assign firewall for %v", requestBody)
	endpoint := "http://" + client.url + "/firewall/assign"
	body, err := json.Marshal(requestBody)
	if err != nil {
		return err
	}
	request, err := http.NewRequest(http.MethodPost, endpoint, bytes.NewBuffer(body))
	if err != nil {
		return err
	}

	request.Header.Set("Content-Type", "application/json")

	response, err := client.httpClient.Do(request)
	if err != nil {
		return err
	}
	if response.StatusCode != http.StatusOK {
		body, err := io.ReadAll(response.Body)
		errMessage := ""
		if err == nil {
			errMessage = string(body)
		}
		return errors.New(errMessage)
	}

	return nil
}

func (client Provider) UnassignFirewall(requestBody provider.AssignFirewallRequest) error {
	logger.DebugLogger().Printf("unassign firewall for %v", requestBody)
	endpoint := "http://" + client.url + "/firewall/unassign"
	body, err := json.Marshal(requestBody)
	if err != nil {
		return err
	}
	request, err := http.NewRequest(http.MethodPost, endpoint, bytes.NewBuffer(body))
	if err != nil {
		return err
	}

	request.Header.Set("Content-Type", "application/json")

	response, err := client.httpClient.Do(request)
	if err != nil {
		return err
	}
	if response.StatusCode != http.StatusOK {
		body, err := io.ReadAll(response.Body)
		errMessage := ""
		if err == nil {
			errMessage = string(body)
		}
		return errors.New(errMessage)
	}

	return nil
}
