package controllers

import (
	"net/http"
	"oakestra-aws-provider/dto"
	"oakestra-aws-provider/logger"
	"oakestra-aws-provider/services"

	"github.com/gin-gonic/gin"
)

type FirewallController struct {
	firewallService services.FirewallService
}

func NewFirewallController(firewallService services.FirewallService) FirewallController {
	return FirewallController{
		firewallService: firewallService,
	}
}

func (controller FirewallController) Add(context *gin.Context) {

	logger.DebugLogger().Printf("Add Firewall")
	var requestBody dto.AssignFirewallRequest
	err := context.ShouldBindJSON(&requestBody)
	if err != nil {
		context.String(http.StatusBadRequest, err.Error())
		return
	}
	logger.DebugLogger().Printf("Request %v", requestBody)
	firewallCreated, err := controller.firewallService.AssignFirewall(requestBody)

	if err != nil {
		context.String(http.StatusBadRequest, err.Error())
		return
	}
	httpStatus := http.StatusOK
	if firewallCreated {
		httpStatus = http.StatusCreated
	}

	context.Status(httpStatus)
}

func (controller FirewallController) Remove(context *gin.Context) {

	logger.DebugLogger().Printf("Remove Firewall")
	firewallId := context.Param("firewallId")
	workerId := context.Param("workerId")

	err := controller.firewallService.UnassignFirewall(firewallId, workerId)
	if err != nil {
		context.String(http.StatusBadRequest, err.Error())
		return
	}

	context.Status(http.StatusOK)
}
