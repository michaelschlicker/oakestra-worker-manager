package controllers

import (
	"net/http"
	"oakestra-aws-provider/logger"

	"github.com/gin-gonic/gin"
)

type HealthController struct{}

func (controller HealthController) Status(context *gin.Context) {
	logger.DebugLogger().Print("Status Health Check")
	context.String(http.StatusOK, "Working!")
}

func (controller HealthController) Test(context *gin.Context) {
	// rootAddress := "54.93.244.81"
	// clusterName := "aws_oakestra_cluster_G"
	// clusterLocation := "aws_eu-central-1b"

	// environment := map[string]string{
	// 	"SYSTEM_MANAGER_URL": rootAddress,
	// 	"CLUSTER_NAME":       clusterName,
	// 	"CLUSTER_LOCATION":   clusterLocation,
	// }
	// installationService := services.InstallationService{}

	// fileContent := installationService.createEnvFile(environment)

	// context.String(http.StatusOK, fileContent.String())

}
