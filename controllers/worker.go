package controllers

import (
	"net/http"
	"oakestra-aws-provider/dto"
	"oakestra-aws-provider/logger"
	"oakestra-aws-provider/services"

	"github.com/gin-gonic/gin"
)

type WorkerController struct {
	workerService services.WorkerService
}

func NewWorkerController(workerService services.WorkerService) WorkerController {
	return WorkerController{
		workerService: workerService,
	}
}

func (controller WorkerController) Create(context *gin.Context) {
	logger.InfoLogger().Printf("WM-CreateWorker-Init")
	var requestBody dto.CreateWorkerRequest
	err := context.ShouldBindJSON(&requestBody)
	if err != nil {
		context.String(http.StatusBadRequest, err.Error())
		return
	}
	logger.DebugLogger().Printf("Create Request: %v", requestBody)
	err = controller.workerService.CreateWorker(requestBody)
	if err != nil {
		context.String(http.StatusInternalServerError, err.Error())
		return
	}
	logger.InfoLogger().Printf("WM-CreateWorker-Created")
	context.String(http.StatusCreated, "")
}

func (controller WorkerController) Status(context *gin.Context) {
	workerId := context.Param("workerId")
	logger.InfoLogger().Printf("WM-WorkerStatus-Init")

	var requestBody dto.SetWorkerStatusRequest
	err := context.ShouldBindJSON(&requestBody)
	if err != nil {
		context.String(http.StatusBadRequest, err.Error())
		return
	}
	logger.InfoLogger().Printf("WM-WorkerStatus-Status-" + requestBody.Status)
	switch requestBody.Status {
	case string(dto.STOP):
		err = controller.workerService.TurnOffWorker(workerId, requestBody.Credentials, false)
	case string(dto.TERMINATE):
		err = controller.workerService.TurnOffWorker(workerId, requestBody.Credentials, true)
	case string(dto.RESTART):
		err = controller.workerService.RestartWorker(workerId, requestBody.Credentials)
	case string(dto.RECREATE):
		err = controller.workerService.RecreateWorker(workerId, requestBody.Credentials)
	default:
		context.String(http.StatusBadRequest, "Invalid Worker State")
	}
	logger.InfoLogger().Printf("WM-WorkerStatus-Done")

	if err != nil {
		context.String(http.StatusInternalServerError, err.Error())
		return
	}

	context.Status(http.StatusOK)
}

func (controller WorkerController) Delete(context *gin.Context) {
	workerId := context.Param("workerId")
	logger.InfoLogger().Printf("WM-WorkerDelete-Init")
	var requestBody dto.Credentials
	err := context.ShouldBindJSON(&requestBody)
	if err != nil {
		context.String(http.StatusBadRequest, err.Error())
		return
	}
	logger.InfoLogger().Printf("WM-WorkerDelete-CredsParsed")
	err = controller.workerService.DeleteWorker(workerId, requestBody.Credentials)
	if err != nil {
		context.String(http.StatusInternalServerError, err.Error())
		return
	}

	context.Status(http.StatusOK)
}

func (controller WorkerController) GetWorkers(context *gin.Context) {
	logger.InfoLogger().Printf("WM-Workerget-Init")
	workers, err := controller.workerService.GetAllWorkers()
	if err != nil {
		context.String(http.StatusInternalServerError, err.Error())
	}
	logger.InfoLogger().Printf("WM-Workerget-WorkersRetrieved")
	context.JSON(http.StatusOK, workers)
}
