package dto

type AssignFirewallRequest struct {
	NodeId string `json:"node_id" binding:"required"`
	JobId  string `json:"job_id" binding:"required"`
}
