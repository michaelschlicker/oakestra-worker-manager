package clustermanager

type RegisterNodeResponse struct {
	WorkerId   string `json:"worker_id"`
	PairingKey string `json:"pairing_key"`
}
