package clustermanager

type ReissueTokenResponse struct {
	PairingKey string `json:"pairing_key"`
}
