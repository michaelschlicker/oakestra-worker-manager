package dto

import "encoding/json"

type CreateWorkerRequest struct {
	InstanceType string          `json:"instance_type" binding:"required"`
	DiskSize     int             `json:"disk_size" binding:"required"`
	ImageId      string          `json:"image_id" binding:"required"`
	WorkerCount  int             `json:"worker_count" binding:"required"`
	Region       string          `json:"region" binding:"required"`
	KeyName      string          `json:"key_name" binding:"omitempty"`
	Credentials  json.RawMessage `json:"credentials" binding:"required"`
}
