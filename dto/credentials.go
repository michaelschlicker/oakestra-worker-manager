package dto

import "encoding/json"

type Credentials struct {
	Credentials json.RawMessage `json:"credentials" binding:"required"`
}
