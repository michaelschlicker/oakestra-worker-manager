package dto

type GetNodeResponse struct {
	ID         string `json:"id"`
	InstanceId string `json:"instance_id"`
	Address    string `json:"node_address"`
	NodeState  string `json:"node_state"`
}
