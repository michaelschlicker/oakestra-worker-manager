package provider

type AssignFirewallRequest struct {
	FirewallID   string       `json:"firewall_id"`
	InstanceID   string       `json:"instance_id"`
	ClusterName  string       `json:"cluster_name" binding:"required"`
	ProviderInfo ProviderInfo `json:"provider_info" binding:"required"`
}
