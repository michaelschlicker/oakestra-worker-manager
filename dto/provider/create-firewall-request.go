package provider

type CreateFirewallRequest struct {
	Port         string       `json:"port"`
	JobId        string       `json:"job_id" binding:"required"`
	ClusterName  string       `json:"cluster_name" binding:"required"`
	ProviderInfo ProviderInfo `json:"provider_info" binding:"required"`
}
