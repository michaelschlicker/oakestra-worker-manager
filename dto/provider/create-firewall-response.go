package provider

type CreateFirewallResponse struct {
	FirewallID string `json:"firewall_id"`
}
