package provider

type CreateWorkerRequest struct {
	InstanceType       string       `json:"instance_type" binding:"required"`
	DiskSize           int          `json:"disk_size" binding:"required"`
	ImageId            string       `json:"image_id" binding:"required"`
	WorkerCount        int          `json:"worker_count" binding:"required"`
	KeyName            string       `json:"key_name" binding:"omitempty"`
	WorkerFirewallID   string       `json:"worker_firewall_id" binding:"required"`
	ProviderInfo       ProviderInfo `json:"provider_info" binding:"required"`
	ClusterName        string       `json:"cluster_name" binding:"required"`
	ClusterAddress     string       `json:"cluster_address" binding:"required"`
	RegistrationTokens []string     `json:"registration_tokens"`
}
