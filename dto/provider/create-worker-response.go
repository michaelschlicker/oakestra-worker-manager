package provider

type CreateWorkerResponse struct {
	InstanceIds []string `json:"instance_ids"`
}
