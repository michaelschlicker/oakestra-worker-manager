package provider

type InstanceRestartRequest struct {
	InstanceId   string       `json:"instance_id" binding:"required"`
	ProviderInfo ProviderInfo `json:"provider_info" binding:"required"`
	KeyName      string       `json:"key_name" binding:"omitempty"`
}
