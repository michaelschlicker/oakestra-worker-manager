package provider

type InstanceStopRequest struct {
	InstanceId   string       `json:"instance_id" binding:"required"`
	ProviderInfo ProviderInfo `json:"provider_info" binding:"required"`
}
