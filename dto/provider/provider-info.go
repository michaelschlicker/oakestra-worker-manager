package provider

import "encoding/json"

type ProviderInfo struct {
	Credentials json.RawMessage `json:"credentials" binding:"required"`
	Region      string          `json:"region" binding:"required"`
	ProjectName string          `json:"project_name" binding:"required"`
}
