package dto

type PublicKey struct {
	Key string `json:"public_key" binding:"required"`
}
