package dto

import "encoding/json"

type SetWorkerStatusRequest struct {
	Status      string          `json:"status" binding:"required"`
	Credentials json.RawMessage `json:"credentials" binding:"required"`
}

type WorkerRequestStatus string

const (
	STOP      WorkerRequestStatus = "STOP"
	TERMINATE WorkerRequestStatus = "TERMINATE"
	RESTART   WorkerRequestStatus = "RESTART"
	RECREATE  WorkerRequestStatus = "RECREATE"
)
