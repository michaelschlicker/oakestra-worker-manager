package logger

import (
	"log"
	"os"
	"sync"
)

var debuglogger *log.Logger
var infologger *log.Logger
var errorlogger *log.Logger
var debugonce sync.Once
var infoonce sync.Once
var erroronce sync.Once

func DebugLogger() *log.Logger {
	debugonce.Do(func() {
		debuglogger = log.New(os.Stdout, "DEBUG-", log.Ldate|log.Lmicroseconds|log.Lshortfile)
	})
	return debuglogger
}

func InfoLogger() *log.Logger {
	infoonce.Do(func() {
		infologger = log.New(os.Stdout, "INFO-", log.Ldate|log.Lmicroseconds|log.Lshortfile)
	})
	return infologger
}

func ErrorLogger() *log.Logger {
	erroronce.Do(func() {
		errorlogger = log.New(os.Stderr, "ERROR-", log.Ldate|log.Lmicroseconds|log.Lshortfile)
	})
	return errorlogger
}
