package main

import (
	"oakestra-aws-provider/server"
)

func main() {
	server.Init()
}
