package models

import "go.mongodb.org/mongo-driver/bson/primitive"

type Job struct {
	ID         primitive.ObjectID `json:"id" bson:"_id,omitempty"`
	FirewallID string             `json:"firewall_id" bson:"firewall_id"`
	Port       string             `json:"port" bson:"port"`
	Instances  []JobInstance      `json:"instance_list" bson:"instance_list"`
}

type JobInstance struct {
	Status         string `json:"status" bson:"status"`
	InstanceNumber int    `json:"instance_number" bson:"instance_number"`
}
