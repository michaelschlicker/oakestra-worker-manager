package models

import "go.mongodb.org/mongo-driver/bson/primitive"

type Node struct {
	ID         primitive.ObjectID `json:"id" bson:"_id"`
	InstanceId string             `bson:"instance_id"`
	Address    string             `bson:"node_address"`
	Config     NodeConfig         `bson:"node_config"`
	NodeState  NodeState          `bson:"node_state"`
}

type NodeConfig struct {
	InstanceType string `bson:"instance_type"`
	KeyName      string `bson:"key_name"`
}

type NodeState string

const (
	REGISTERED  NodeState = "REGISTERED"
	BOOTING     NodeState = "BOOTING"
	ACTIVE      NodeState = "ACTIVE"
	FAILED      NodeState = "FAILED"
	TERMINATING NodeState = "TERMINATING"
	SHUTDOWN    NodeState = "SHUTDOWN"
	STOPPED     NodeState = "STOPPED"
)
