package repositories

import (
	"context"
	"fmt"
	"oakestra-aws-provider/logger"
	"oakestra-aws-provider/models"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

type JobsRepo struct {
	jobsCollection *mongo.Collection
}

func NewJobsRepo(mongoClient *mongo.Client) JobsRepo {
	return JobsRepo{
		jobsCollection: mongoClient.Database("jobs").Collection("jobs"),
	}
}

func (repo JobsRepo) GetJob(jobIdString string) (models.Job, error) {
	jobId, err := primitive.ObjectIDFromHex(jobIdString)
	if err != nil {
		return models.Job{}, err
	}
	filter := bson.M{
		"_id": jobId,
	}
	var job models.Job
	err = repo.jobsCollection.FindOne(context.TODO(), filter).Decode(&job)
	if err != nil {
		return job, err
	}

	return job, nil
}

func (repo JobsRepo) SetFirewallID(jobIdString string, firewallId string) error {
	jobId, err := primitive.ObjectIDFromHex(jobIdString)
	if err != nil {
		return err
	}
	filter := bson.M{
		"_id": jobId,
	}
	update := bson.M{
		"$set": bson.M{
			"firewall_id": firewallId,
		},
	}
	result, err := repo.jobsCollection.UpdateOne(context.TODO(), filter, update)
	if err != nil {
		return err
	}
	logger.DebugLogger().Printf("job set firewall Id result %v", result)
	if result.ModifiedCount != 1 {
		return fmt.Errorf("didn't set one firewall id as it matches with %v jobs", result.MatchedCount)
	}
	return err
}
