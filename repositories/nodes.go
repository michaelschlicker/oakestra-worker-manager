package repositories

import (
	"context"
	"fmt"
	"oakestra-aws-provider/logger"
	"oakestra-aws-provider/models"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

type NodesRepo struct {
	nodesCollection *mongo.Collection
}

func NewNodesRepo(mongoClient *mongo.Client) NodesRepo {
	return NodesRepo{
		nodesCollection: mongoClient.Database("nodes").Collection("nodes"),
	}
}

func (repo NodesRepo) GetNodes() ([]models.Node, error) {
	result, err := repo.nodesCollection.Find(context.TODO(), bson.D{})
	if err != nil {
		return []models.Node{}, err
	}
	var allNodes []models.Node
	err = result.All(context.TODO(), allNodes)
	if err != nil {
		return []models.Node{}, err
	}
	return allNodes, nil
}

func (repo NodesRepo) GetNode(nodeIdString string) (models.Node, error) {
	jobId, err := primitive.ObjectIDFromHex(nodeIdString)
	if err != nil {
		return models.Node{}, err
	}
	filter := bson.M{
		"_id": jobId,
	}
	var node models.Node
	err = repo.nodesCollection.FindOne(context.TODO(), filter).Decode(&node)
	if err != nil {
		return node, err
	}

	return node, nil
}

func (repo NodesRepo) SetNodesBootingWithConfig(nodeIdStrings []string, config models.NodeConfig) error {
	nodeIds, err := CreateObjectIDs(nodeIdStrings)
	if err != nil {
		return err
	}
	filter := bson.M{
		"_id": bson.M{
			"$in": nodeIds,
		},
		"node_state": bson.M{
			"$in": []models.NodeState{models.SHUTDOWN, models.REGISTERED},
		},
	}
	update := bson.M{
		"$set": bson.M{
			"node_config": config,
			"node_state":  models.BOOTING,
		},
	}
	result, err := repo.nodesCollection.UpdateMany(context.TODO(), filter, update)
	if err != nil {
		return err
	}
	logger.DebugLogger().Printf("upsert result: %v", result)
	if int(result.ModifiedCount) != len(nodeIdStrings) {
		return fmt.Errorf("node booting modified count %v doesn't equal nodeIds count %v while it found %v nodes", result.ModifiedCount, len(nodeIdStrings), result.MatchedCount)
	}
	return nil
}

func (repo NodesRepo) SetNodeTerminating(nodeIdString string) error {
	nodeId, err := primitive.ObjectIDFromHex(nodeIdString)
	if err != nil {
		return err
	}
	filter := bson.M{
		"_id": nodeId,
		"node_state": bson.M{
			"$in": []models.NodeState{models.ACTIVE, models.FAILED},
		},
	}
	update := bson.M{
		"$set": bson.M{
			"node_state": models.TERMINATING,
		},
	}
	result, err := repo.nodesCollection.UpdateOne(context.TODO(), filter, update)
	if err != nil {
		return err
	}
	logger.DebugLogger().Printf("set terminating result: %v", result)
	if int(result.ModifiedCount) != 1 {
		return fmt.Errorf("node terminating modified count %v isn't one while it found %v nodes", result.ModifiedCount, result.MatchedCount)
	}
	return nil
}

func (repo NodesRepo) SetNodeShutdown(nodeIdString string) error {
	nodeId, err := primitive.ObjectIDFromHex(nodeIdString)
	if err != nil {
		return err
	}
	filter := bson.M{
		"_id": nodeId,
		"node_state": bson.M{
			"$in": []models.NodeState{models.TERMINATING},
		},
	}
	update := bson.M{
		"$set": bson.M{
			"node_state": models.SHUTDOWN,
		},
	}
	result, err := repo.nodesCollection.UpdateOne(context.TODO(), filter, update)
	if err != nil {
		return err
	}
	logger.DebugLogger().Printf("set terminating result: %v", result)
	if int(result.ModifiedCount) != 1 {
		return fmt.Errorf("node shutdown modified count %v isn't one while it found %v nodes", result.ModifiedCount, result.MatchedCount)
	}
	return err
}

func (repo NodesRepo) SetNodeStopped(nodeIdString string) error {
	nodeId, err := primitive.ObjectIDFromHex(nodeIdString)
	if err != nil {
		return err
	}
	filter := bson.M{
		"_id": nodeId,
		"node_state": bson.M{
			"$in": []models.NodeState{models.TERMINATING},
		},
	}
	update := bson.M{
		"$set": bson.M{
			"node_state": models.STOPPED,
		},
	}
	result, err := repo.nodesCollection.UpdateOne(context.TODO(), filter, update)
	if err != nil {
		return err
	}
	logger.DebugLogger().Printf("set terminating result: %v", result)
	if int(result.ModifiedCount) != 1 {
		return fmt.Errorf("node shutdown modified count %v isn't one while it found %v nodes", result.ModifiedCount, result.MatchedCount)
	}
	return err
}

func (repo NodesRepo) DeleteNode(nodeIdString string) error {
	nodeId, err := primitive.ObjectIDFromHex(nodeIdString)
	if err != nil {
		return err
	}
	filter := bson.M{
		"_id": nodeId,
		"node_state": bson.M{
			"$in": []models.NodeState{models.SHUTDOWN},
		},
	}
	result, err := repo.nodesCollection.DeleteOne(context.TODO(), filter)
	if err != nil {
		return err
	}
	logger.DebugLogger().Printf("set terminating result: %v", result)
	if int(result.DeletedCount) != 1 {
		return fmt.Errorf("node shutdown modified count %v isn't one", result.DeletedCount)
	}
	return err
}

func (repo NodesRepo) SetInstanceIds(nodeIdStrings []string, instanceIds []string) error {
	if len(nodeIdStrings) != len(instanceIds) {
		return fmt.Errorf(`number of Node Ids %v is unequal to the number of instance ids %v`, len(nodeIdStrings), len(instanceIds))
	}
	nodeIds, err := CreateObjectIDs(nodeIdStrings)
	if err != nil {
		return err
	}
	writeModels := make([]mongo.WriteModel, len(nodeIds))
	for i, nodeId := range nodeIds {
		instanceId := instanceIds[i]
		writeModels[i] = mongo.NewUpdateOneModel().SetFilter(bson.M{"_id": nodeId}).SetUpdate(bson.M{"$set": bson.M{
			"instance_id": instanceId,
		}})
	}

	bulkResult, err := repo.nodesCollection.BulkWrite(context.TODO(), writeModels)
	if err != nil {
		return err
	}
	logger.DebugLogger().Printf("Provider Id Setting result %v", bulkResult)
	if int(bulkResult.ModifiedCount) != len(nodeIdStrings) {
		return fmt.Errorf("node set instance id modified count %v doesn't equal nodeIds count %v as it found %v nodes", bulkResult.ModifiedCount, len(nodeIdStrings), bulkResult.MatchedCount)
	}

	return err
}
