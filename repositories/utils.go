package repositories

import "go.mongodb.org/mongo-driver/bson/primitive"

func CreateObjectIDs(idStrings []string) ([]primitive.ObjectID, error) {
	ids := make([]primitive.ObjectID, len(idStrings))
	for i, idString := range idStrings {
		nodeId, err := primitive.ObjectIDFromHex(idString)
		if err != nil {
			return []primitive.ObjectID{}, err
		}
		ids[i] = nodeId
	}
	return ids, nil
}
