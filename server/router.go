package server

import (
	"net/http"
	"oakestra-aws-provider/clients"
	"oakestra-aws-provider/controllers"
	"oakestra-aws-provider/repositories"

	"oakestra-aws-provider/services"

	"github.com/gin-gonic/gin"
	"go.mongodb.org/mongo-driver/mongo"
)

func NewRouter(httpClient *http.Client, mongoClient *mongo.Client) *gin.Engine {
	router := gin.New()

	nodesRepo := repositories.NewNodesRepo(mongoClient)
	jobsRepo := repositories.NewJobsRepo(mongoClient)

	provider := clients.NewProvider(httpClient)
	clusterManager := clients.NewClusterManager(httpClient)

	workerService := services.NewWorkerService(clusterManager, provider, nodesRepo)
	firewallService := services.NewFirewallService(provider, nodesRepo, jobsRepo)

	health := new(controllers.HealthController)
	worker := controllers.NewWorkerController(workerService)
	firewall := controllers.NewFirewallController(firewallService)

	router.GET("/health", health.Status)
	router.POST("/health", health.Test)

	router.GET("/workers", worker.GetWorkers)
	router.POST("/worker", worker.Create)
	router.POST("/worker/:workerId/status", worker.Status)
	router.DELETE("/worker/:workerId", worker.Delete)

	router.POST("/firewall", firewall.Add)
	router.DELETE("/firewall/:firewallId/worker/:workerId", firewall.Remove)

	return router
}
