package server

import (
	"context"
	"log"
	"net/http"
	"oakestra-aws-provider/logger"
	"os"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

func Init() {
	httpClient := &http.Client{}

	mongoClient, err := mongo.Connect(context.Background(), options.Client().ApplyURI("mongodb://"+os.Getenv("DB_HOST")+":"+os.Getenv("DB_PORT")))
	if err != nil {
		log.Fatalf("Fatal: Could not connect to MongoDB: %v", err.Error())
	}
	defer func() {
		if err := mongoClient.Disconnect(context.Background()); err != nil {
			logger.DebugLogger().Printf("Failed to disconnect MongoDB: %v", err.Error())
		}
	}()
	r := NewRouter(httpClient, mongoClient)
	r.Run()
}
