package services

import (
	"encoding/base64"
	"encoding/json"
	"oakestra-aws-provider/clients"
	"oakestra-aws-provider/dto"
	"oakestra-aws-provider/dto/provider"
	"oakestra-aws-provider/logger"
	"oakestra-aws-provider/repositories"
	"os"
)

type FirewallService struct {
	provider  clients.Provider
	nodesRepo repositories.NodesRepo
	jobsRepo  repositories.JobsRepo
}

func NewFirewallService(provider clients.Provider, nodesRepo repositories.NodesRepo, jobsRepo repositories.JobsRepo) FirewallService {
	return FirewallService{
		provider:  provider,
		nodesRepo: nodesRepo,
		jobsRepo:  jobsRepo,
	}
}

func (service FirewallService) getProviderInfo() (provider.ProviderInfo, error) {
	credenitalsString := os.Getenv("CLUSTER_USER_CREDS")
	region := os.Getenv("REGION_NAME")
	projectName := os.Getenv("PROJECT_NAME")

	decodedCredentials, err := base64.StdEncoding.DecodeString(credenitalsString)
	if err != nil {
		return provider.ProviderInfo{}, err
	}

	return provider.ProviderInfo{
		Credentials: json.RawMessage(decodedCredentials),
		Region:      region,
		ProjectName: projectName,
	}, nil
}

func (service FirewallService) AssignFirewall(request dto.AssignFirewallRequest) (bool, error) {

	providerInfo, err := service.getProviderInfo()
	if err != nil {
		return false, err
	}
	clusterName := os.Getenv("CLUSTER_NAME")
	logger.DebugLogger().Printf("clusterName %v", clusterName)

	logger.DebugLogger().Printf("Provider Info %v", providerInfo)
	job, err := service.jobsRepo.GetJob(request.JobId)
	if err != nil {
		return false, err
	}
	logger.DebugLogger().Printf("JOb %v", job)
	firewallID := job.FirewallID
	if firewallID == "" {
		createFirewallRequest := provider.CreateFirewallRequest{
			Port:         job.Port,
			JobId:        request.JobId,
			ProviderInfo: providerInfo,
			ClusterName:  clusterName,
		}
		firewallID, err = service.provider.CreateFirewall(createFirewallRequest)
		if err != nil {
			return false, err
		}
		err = service.jobsRepo.SetFirewallID(request.JobId, firewallID)
		if err != nil {
			return false, err
		}
	}
	logger.DebugLogger().Printf("firewallID %v", firewallID)
	node, err := service.nodesRepo.GetNode(request.NodeId)
	if err != nil {
		return false, err
	}
	logger.DebugLogger().Printf("Node %v", node)
	assignFirewallRequest := provider.AssignFirewallRequest{
		FirewallID:   firewallID,
		InstanceID:   node.InstanceId,
		ProviderInfo: providerInfo,
		ClusterName:  clusterName,
	}
	logger.DebugLogger().Printf("Assignment Req %v", assignFirewallRequest)
	err = service.provider.AssignFirewall(assignFirewallRequest)
	if err != nil {
		return false, err
	}

	return job.FirewallID == "", nil
}

func (service FirewallService) UnassignFirewall(jobId string, workerId string) error {
	logger.DebugLogger().Printf("Unassign firewall for job %v and worker %v", jobId, workerId)
	providerInfo, err := service.getProviderInfo()
	if err != nil {
		return err
	}
	clusterName := os.Getenv("CLUSTER_NAME")
	logger.DebugLogger().Printf("Cluster %v provider Info %v", clusterName, providerInfo)
	job, err := service.jobsRepo.GetJob(jobId)
	if err != nil {
		return err
	}
	logger.DebugLogger().Printf("Found Job %v", job)
	node, err := service.nodesRepo.GetNode(workerId)
	if err != nil {
		return err
	}
	logger.DebugLogger().Printf("Found Node %v", node)

	unassignFiewallRequest := provider.AssignFirewallRequest{
		FirewallID:   job.FirewallID,
		InstanceID:   node.InstanceId,
		ProviderInfo: providerInfo,
		ClusterName:  clusterName,
	}

	return service.provider.UnassignFirewall(unassignFiewallRequest)
}
