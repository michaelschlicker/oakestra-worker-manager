package services

import (
	"encoding/json"
	"errors"
	"fmt"
	"oakestra-aws-provider/clients"
	"oakestra-aws-provider/dto"
	"oakestra-aws-provider/dto/provider"
	"oakestra-aws-provider/logger"
	"oakestra-aws-provider/models"
	"oakestra-aws-provider/repositories"
	"os"
	"strconv"
)

type WorkerService struct {
	provider       clients.Provider
	clusterManager clients.ClusterManager
	nodesRepo      repositories.NodesRepo
}

func NewWorkerService(clusterManager clients.ClusterManager, provider clients.Provider, nodesRepo repositories.NodesRepo) WorkerService {
	return WorkerService{
		clusterManager: clusterManager,
		provider:       provider,
		nodesRepo:      nodesRepo,
	}
}

func (service WorkerService) CreateWorker(requestBody dto.CreateWorkerRequest) error {
	// Add request to Cluster Manager instance type
	// Get ID & token set to request with
	logger.DebugLogger().Printf("Create Worker Service")
	nodeRegistrations, err := service.clusterManager.RegisterNodes(requestBody.WorkerCount)
	if err != nil {
		return err
	}

	logger.InfoLogger().Printf("WM-CreateWorker-NodesRegistered")
	logger.DebugLogger().Printf("Registered Nodes %v", nodeRegistrations)
	nodeConfig := models.NodeConfig{
		InstanceType: requestBody.InstanceType,
		KeyName:      requestBody.KeyName,
	}
	nodeIds := []string{}
	registrationTokens := []string{}
	for _, registration := range nodeRegistrations {
		nodeIds = append(nodeIds, registration.WorkerId)
		registrationTokens = append(registrationTokens, registration.PairingKey)
	}

	// Set Node Info & Booting
	err = service.nodesRepo.SetNodesBootingWithConfig(nodeIds, nodeConfig)
	if err != nil {
		return err
	}
	logger.InfoLogger().Printf("WM-CreateWorker-NodesModeBooting")
	logger.DebugLogger().Printf("Set Node Config %v for node ids %v", nodeConfig, nodeIds)

	var providerRequest = provider.CreateWorkerRequest{
		InstanceType:       requestBody.InstanceType,
		ImageId:            requestBody.ImageId,
		DiskSize:           requestBody.DiskSize,
		WorkerCount:        requestBody.WorkerCount,
		ProviderInfo:       getProviderInfo(requestBody.Credentials),
		KeyName:            requestBody.KeyName,
		WorkerFirewallID:   os.Getenv("WORKER_FIREWALL_ID"),
		ClusterName:        os.Getenv("CLUSTER_NAME"),
		ClusterAddress:     os.Getenv("CLUSTER_ADDRESS"),
		RegistrationTokens: registrationTokens,
	}
	logger.InfoLogger().Printf("WM-CreateWorker-ProviderStart")
	instanceIds, err := service.provider.CreateWorker(providerRequest)
	if err != nil {
		return err
	}
	logger.InfoLogger().Printf("WM-CreateWorker-ProviderDone")
	logger.DebugLogger().Printf("Worker Instances created: %v", instanceIds)

	return service.nodesRepo.SetInstanceIds(nodeIds, instanceIds)
}

func (service WorkerService) TurnOffWorker(workerId string, credentials json.RawMessage, shouldTerminate bool) error {
	logger.InfoLogger().Printf("WM-TurnOffWorker-Terminate-" + strconv.FormatBool(shouldTerminate))
	err := service.nodesRepo.SetNodeTerminating(workerId)
	if err != nil {
		return err
	}
	logger.InfoLogger().Printf("WM-TurnOffWorker-SetTerminating")
	node, err := service.nodesRepo.GetNode(workerId)
	if err != nil {
		return err
	}
	logger.InfoLogger().Printf("WM-TurnOffWorker-NodeRetrieved")
	if node.NodeState != models.TERMINATING {
		return fmt.Errorf("node %v is in state %v instead of TERMINATING", workerId, node.NodeState)
	}

	err = service.clusterManager.UndeployNode(workerId)
	if err != nil {
		return err
	}
	logger.InfoLogger().Printf("WM-TurnOffWorker-NodeUndeployed")

	// TODO: Wait for successful redepoloy
	requestBody := provider.InstanceStopRequest{
		ProviderInfo: getProviderInfo(credentials),
		InstanceId:   node.InstanceId,
	}

	if shouldTerminate {
		logger.InfoLogger().Printf("WM-TurnOffWorker-DeleteWorkerStart")
		err = service.provider.DeleteWorker(requestBody)
		if err != nil {
			return err
		}
		logger.InfoLogger().Printf("WM-TurnOffWorker-DeleteWorkerDone")
		service.nodesRepo.SetNodeShutdown(workerId)
		logger.InfoLogger().Printf("WM-TurnOffWorker-SetShutdown")
	} else {
		logger.InfoLogger().Printf("WM-TurnOffWorker-StopWorkerStart")
		err = service.provider.StopWorker(requestBody)
		if err != nil {
			return err
		}
		logger.InfoLogger().Printf("WM-TurnOffWorker-StopWorkerDone")
		service.nodesRepo.SetNodeStopped(workerId)
		logger.InfoLogger().Printf("WM-TurnOffWorker-SetStopped")
	}
	// Optional @Node shut down gracefully
	// @Provider Terminate Instance
	// Set Database to turned off state
	return nil
}

func (service WorkerService) RestartWorker(workerId string, credentials json.RawMessage) error {
	logger.InfoLogger().Printf("WM-RestartWorker-Start")
	node, err := service.nodesRepo.GetNode(workerId)
	if err != nil {
		return err
	}
	logger.InfoLogger().Printf("WM-RestartWorker-GotNode")

	request := provider.InstanceRestartRequest{
		InstanceId:   node.InstanceId,
		ProviderInfo: getProviderInfo(credentials),
		KeyName:      node.Config.KeyName,
	}
	err = service.provider.RestartWorker(request)
	logger.InfoLogger().Printf("WM-RestartWorker-Restarted")
	return err
}

func (service WorkerService) RecreateWorker(workerId string, credentials json.RawMessage) error {
	logger.InfoLogger().Printf("WM-RecreateWorker-Start")
	reissuedToken, err := service.clusterManager.ReissueToken(workerId)
	if err != nil {
		return err
	}
	logger.InfoLogger().Printf("WM-RecreateWorker-TokenReIssued")

	node, err := service.nodesRepo.GetNode(workerId)
	if err != nil {
		return err
	}
	logger.InfoLogger().Printf("WM-RecreateWorker-NodeRetrieved")

	// Set Node Info & Booting
	err = service.nodesRepo.SetNodesBootingWithConfig([]string{workerId}, node.Config)
	if err != nil {
		return err
	}
	logger.InfoLogger().Printf("WM-RecreateWorker-SetBooting")

	var providerRequest = provider.CreateWorkerRequest{
		InstanceType:       node.Config.InstanceType,
		WorkerCount:        1,
		ProviderInfo:       getProviderInfo(credentials),
		KeyName:            node.Config.KeyName,
		WorkerFirewallID:   os.Getenv("WORKER_FIREWALL_ID"),
		ClusterName:        os.Getenv("CLUSTER_NAME"),
		ClusterAddress:     os.Getenv("CLUSTER_ADDRESS"),
		RegistrationTokens: []string{reissuedToken.PairingKey},
	}
	logger.InfoLogger().Printf("WM-RecreateWorker-ProviderStart")
	instanceIds, err := service.provider.CreateWorker(providerRequest)
	if err != nil {
		return err
	}
	if len(instanceIds) != 1 {
		return errors.New("Recreating worker " + workerId + " has resulted not in 1 but " + strconv.Itoa(len(instanceIds)) + " instanceIds")
	}
	logger.InfoLogger().Printf("WM-RecreateWorker-ProviderDone")
	logger.DebugLogger().Printf("Worker Instances created: %v", instanceIds)

	err = service.nodesRepo.SetInstanceIds([]string{workerId}, instanceIds)
	logger.InfoLogger().Printf("WM-RecreateWorker-InstanceIdSet")
	return err
}

func (service WorkerService) DeleteWorker(workerId string, credentials json.RawMessage) error {
	err := service.TurnOffWorker(workerId, credentials, true)
	if err != nil {
		return err
	}
	return service.nodesRepo.DeleteNode(workerId)
}

func (service WorkerService) GetAllWorkers() ([]dto.GetNodeResponse, error) {
	workers, err := service.nodesRepo.GetNodes()
	if err != nil {
		return []dto.GetNodeResponse{}, err
	}
	logger.InfoLogger().Printf("WM-Workerget-GotFromDB")
	var responses []dto.GetNodeResponse
	for _, worker := range workers {
		responses = append(responses, dto.GetNodeResponse{
			ID:         worker.ID.String(),
			InstanceId: worker.InstanceId,
			Address:    worker.Address,
			NodeState:  string(worker.NodeState),
		})
	}
	return responses, nil
}

func getProviderInfo(credentials json.RawMessage) provider.ProviderInfo {
	return provider.ProviderInfo{
		ProjectName: os.Getenv("PROJECT_NAME"),
		Region:      os.Getenv("REGION_NAME"),
		Credentials: credentials,
	}
}
